import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
 import {AuthGuard} from './auth/auth.guard';
 import { AuthInterceptor } from './auth/auth.interceptor';
// materil design
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material';
import { ComponentnameComponent } from './componentname/componentname.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { from, observable } from 'rxjs';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { MainNavComponent } from './main-nav/main-nav.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';






import {UsersService} from './users.service';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { ProblemPostingComponent } from './components/problem-posting/problem-posting.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { TeamComponent } from './team/team.component';

// loaing
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSpinnerModule } from 'ngx-spinner';

// file upload
import { MatFileUploadModule } from 'angular-material-fileupload';
import { AchievementsComponent } from './components/achievements/achievements.component';
// flipping card
import { FlipModule } from 'ngx-flip';

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { AchievementPostComponent } from './components/achievement-post/achievement-post.component';
import { OtpverifyComponent } from './components/otpverify/otpverify.component';
import { AccountinfoComponent } from './components/accountinfo/accountinfo.component';
import { AccountinfoPostComponent } from './components/accountinfo-post/accountinfo-post.component';
import { FaqComponent } from './components/faq/faq.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { ListOfUsersComponent } from './components/list-of-users/list-of-users.component';
import { TodayspecialPostComponent } from './components/todayspecial-post/todayspecial-post.component';
import { BalanceUpdateComponent } from './components/balance-update/balance-update.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { EventsComponent } from './components/events/events.component';
import { ProblemsComponent } from './components/problems/problems.component';
import { AccsheetComponent } from './components/accsheet/accsheet.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentnameComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutusComponent,
    ContactusComponent,
    MyNavComponent,
    MainNavComponent,
    RegisterComponent,
    LoginComponent,
    ProblemPostingComponent,
    UserProfileComponent,
    TeamComponent,
    AchievementsComponent,
    AchievementPostComponent,
    OtpverifyComponent,
    AccountinfoComponent,
    AccountinfoPostComponent,
    FaqComponent,
    AdminDashboardComponent,
    ListOfUsersComponent,
    TodayspecialPostComponent,
    BalanceUpdateComponent,
    GalleryComponent,
    EventsComponent,
    ProblemsComponent,
    AccsheetComponent,
    
   

  ],
  imports: [

    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    // spinner loading
    NgxSpinnerModule,
    // fire base
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyBmnCO9E7Jt_eecG8toMbvfD4g4VJ5Kmt8',
    authDomain: 'hho-rkv.firebaseapp.com',
    storageBucket: 'hho-rkv.appspot.com',
    databaseURL: 'https://hho-rkv.firebaseio.com',
    projectId: 'hho-rkv',
    messagingSenderId: '355204344788'

    }),
    AngularFireStorageModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      {path: 'aboutus', component: AboutusComponent},
      {path: 'contactus', component: ContactusComponent},
      {path: 'home', component: HomeComponent},
      {path: 'my-nav', component: MyNavComponent},
      {path: 'main- nav', component: MainNavComponent},
      {path : 'register', component: RegisterComponent},
      {path: 'login', component: LoginComponent},
      {path: 'post', component: ProblemPostingComponent,  canActivate: [AuthGuard]},
      {path: 'user_profile', component: UserProfileComponent, canActivate: [AuthGuard]},
      {path : 'team', component: TeamComponent },
      {path: 'achievements', component: AchievementsComponent},
      // {path: 'achieve_post', component: AchievementPostComponent},
      {path: 'otp_verify', component: OtpverifyComponent},
      {path: 'accinfo', component: AccountinfoComponent},
      // {path: 'accinfo_post', component: AccountinfoPostComponent },
      {path: 'faqs', component: FaqComponent},
      {path: 'admin', component: AdminDashboardComponent,
          children: [
            {path: 'achieve_post', component: AchievementPostComponent},
            {path: 'accinfo_post', component: AccountinfoPostComponent },
            {path: 'users_list', component: ListOfUsersComponent },
            {path: 'todayspecial_post', component: TodayspecialPostComponent },
            {path: 'balance_update', component: BalanceUpdateComponent },
            {path: 'problems_list', component: ProblemsComponent },
            {path: 'accsheet', component: AccsheetComponent }
          ]
    },
    {path: 'gallery', component: GalleryComponent},
    {path: 'events', component: EventsComponent},
    ]),
    NgxLoadingModule.forRoot({}),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFileUploadModule,
    FlipModule,
    MatTableModule,
    MatChipsModule,
    MatSidenavModule,
    MatDialogModule
   
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }, AuthGuard, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
