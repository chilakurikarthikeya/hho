import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import {UsersService} from '../users.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

 
    constructor(private router: Router, private usersService: UsersService) { }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
     if(!this.usersService.isLoggedIn()) {
       this.router.navigateByUrl('/login');
       this.usersService.deleteToken();
     }
      return true;
  }
}
