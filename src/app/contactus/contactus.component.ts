import { Component, OnInit } from '@angular/core';
import {UsersService} from '../users.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  firstname: String;
  lastname: String;
  sender_phno: String;
  sender_email: string;
  sender_subject: String;
  sender_message: String;
  mail;

  constructor(private userservice: UsersService, private router: Router) { }

  ngOnInit() {
  }

  onpost() {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    if(this.firstname === undefined || this.lastname === undefined || this.sender_phno === undefined ||
      this.sender_email === undefined || this.sender_subject === undefined || this.sender_message === undefined){
        alert('fille all fields');
        return ;
      } else {

     if (re.test(this.sender_email)){
      this.mail = {
        sender_firstname: this.firstname,
      sender_lastname: this.lastname,
      sender_phno: this.sender_phno,
      sender_email: this.sender_email,
      sender_subject: this.sender_subject,
      sender_message: this.sender_message
      };
     } else {
       alert('invalid mail');
       return;
     }
    }

    this.userservice.contactus( this.mail).subscribe(
      res => {
          alert('success');
      },
      err => {

        alert('failure');
      }
    );


  }

}
