import { Component, OnInit } from '@angular/core';
import {UsersService} from '../users.service';
import {AuthGuard} from '../auth/auth.guard';
import {Router, RouterModule} from '@angular/router';

export interface Food {
  value: string;
  viewValue: string;
}

// let login: Boolean;
// if ( this.usersService.isLoggedIn()) {

//   login = true;

// } else {
//   login = false;
// }

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})




export class HeaderComponent {

  constructor( private usersService: UsersService, private router: Router) { }
  events: string[] = [];
  opened: boolean;
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'HR'},
    {value: 'pizza-1', viewValue: 'non-HR'},
    {value: 'tacos-2', viewValue: 'Victim'}
  ];


  onLogout() {
    this.usersService.deleteToken();
    this.router.navigate(['/login']);

  }
}

