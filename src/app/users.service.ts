import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {environment} from '../environments/environment';

import {tokenNotExpired} from 'angular2-jwt';

import {HR, Student, User} from './user.model';
@Injectable({
  providedIn: 'root'
})
export class UsersService {
 
    // selectedHR: HR = {
    //   fullname: '',
    // email: '',
    // id: '',
    // ph_no: '',
    // class: '',
    // branch: '',
    // moto_of_joining: '',
    // date_of_joining: '',
    // designatio: '',
    // password: '',
    // c_password: ''
    // };
    // selectedStudent: Student = {
    //   fullname: '',
    // email: '',
    // id: '',
    // ph_no: '',
    // class: '',
    // branch: '',
    // password: '',
    // c_password: ''
    // };
    // selectedUser: User = {
    //   fullname: '',
    // email: '',
    // ph_no: '',
    // password: '',
    // c_password: ''
    // };


    otp_verify;
    acc_balance;



  constructor( private http: HttpClient) { }



 noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  registerHR(hr) {
    return this.http.post(environment.apiBaseUrl + '/register', hr, this.noAuthHeader);

  }
  registerStu(Stu ) {
    // alert('called');
      return this.http.post(environment.apiBaseUrl + '/stu_register', Stu, this.noAuthHeader);
  }
  registerOther(Other ) {
    // alert('called');
      return this.http.post(environment.apiBaseUrl + '/others_register', Other, this.noAuthHeader);
  }

  login(authCredentials) {
    return this.http.post(environment.apiBaseUrl + '/authenticate', authCredentials, this.noAuthHeader);
  }
  getUserProfile() {
    return this.http.get(environment.apiBaseUrl + '/userProfile');
  }

  achievepost(post) {
    // alert('called');
    return this.http.post(environment.apiBaseUrl + '/achievementpost', post, this.noAuthHeader);
  }
  achievements() {
    // alert('in achievements');
    return this.http.get(environment.apiBaseUrl + '/achievements', this.noAuthHeader);
  }
  // get users
  hrs_list() {
    // alert('in hrslist');
    return this.http.get(environment.apiBaseUrl + '/hrs_list', this.noAuthHeader);

  }
  // UPDATE HR
  approve_hr(id) {
    
    return this.http.put(environment.apiBaseUrl + '/hr_verify/' + id, this.noAuthHeader);

  }
  // admin verify
  admin_verify(){
   
    return this.http.get(environment.apiBaseUrl + '/admin_verify');

  }

  // send_mail
  send_mail(otp) {
    console.log('called in send_mail');

    console.log(otp);
    return this.http.post(environment.apiBaseUrl + '/send_mail', otp);
  }

  // otp
  otp() {
    console.log('called in service');

    return this.http.get(environment.apiBaseUrl + '/otp');
  }
  setToken(token: string) {
    localStorage.setItem('token', token);
  }
  getToken() {
    return localStorage.getItem('token');
  }

  // problem post

  problempost(problem) {
      console.log('called problem post in services');
      alert(problem.doc_path);
     return this.http.post(environment.apiBaseUrl + '/problem_post', problem);
  }

   // get problem list

   problems_list() {
    console.log('called problem post in services');
   return this.http.get(environment.apiBaseUrl + '/problems_list');
}
 // update problem status

 problem_status_update(id, status) {
  console.log('called problem post in services');
 return this.http.post(environment.apiBaseUrl + '/problem_status_update/' + id, status);
}


  // accinfo_post
  accinfo_post(accinfo) {
    return this.http.post(environment.apiBaseUrl + '/accinfo_post', accinfo);
  }

  // today special post
  todayspecial_post(special) {
   console.log(special);
    return this.http.post(environment.apiBaseUrl + '/TodaySpecial_post', special);
  }

  // get today special 
  todayspecial() {
    return this.http.get(environment.apiBaseUrl + '/TodaySpecial', this.noAuthHeader);
  }
  // accinfo
  accinfo() {
    return this.http.get(environment.apiBaseUrl + '/accinfo', this.noAuthHeader);
  }

  // get acc balance
  getacc_balance(){
    return this.http.get(environment.apiBaseUrl + '/accbalance_get', this.noAuthHeader);

  }

  // update acc balance
  updateacc_balance(money){
    return this.http.post(environment.apiBaseUrl + '/accbalance_update', money);

  }

  // get accsheet
  accsheet_get(){
    return this.http.get(environment.apiBaseUrl + '/accsheet_get', this.noAuthHeader);

  }
  // pst accsheet
  accsheet_post(sheet) {
    return this.http.post(environment.apiBaseUrl + '/accsheet_post', sheet );

  }
  // contact us form

  contactus(mail) {
    return this.http.post(environment.apiBaseUrl + '/contactus', mail, this.noAuthHeader);
  }
  

  // for hide buttons
  loggedIn() {
    return tokenNotExpired();
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  getUserPayload() {
    var token=this.getToken();

    if (token) {
       var userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    } else {
      return null;
    }

  }


  isLoggedIn() {
    var userPayload = this.getUserPayload();
    if (userPayload)
      return userPayload.exp > Date.now() / 1000;
    else
      return false;
  }

  // loggedIn() {
  //   return tokenNotExpired('id_token');
  // }


}
