import {MatButtonModule} from '@angular/material';
import { NgModule } from '@angular/core';

import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
// import {MatDatepickerModule} from '@angular/material/datepicker';
// import {FormControl, Validators} from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';
// import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonToggleModule} from '@angular/material/button-toggle';


import {

  MatBadgeModule,
  MatBottomSheetModule,
  MatDialogModule,
  MatDividerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSliderModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';




@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatMenuModule, MatIconModule, MatCardModule,
    MatSidenavModule, MatToolbarModule, MatListModule, MatChipsModule, MatRippleModule,
    MatFormFieldModule, MatExpansionModule, MatDatepickerModule,MatSelectModule,MatAutocompleteModule,MatInputModule,MatDatepickerModule,
    MatTabsModule,MatGridListModule, MatDatepickerModule, MatSlideToggleModule, MatButtonToggleModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatDialogModule,
    MatDividerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSliderModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTooltipModule,
    MatTreeModule],

  exports: [MatButtonModule, MatCheckboxModule, MatMenuModule, MatIconModule, MatCardModule,
     MatSidenavModule, MatToolbarModule, MatListModule, MatChipsModule, MatRippleModule,
     MatFormFieldModule, MatExpansionModule, MatDatepickerModule,MatSelectModule,MatAutocompleteModule,
     MatInputModule,MatDatepickerModule,MatTabsModule,MatGridListModule, MatDatepickerModule,
      MatSlideToggleModule, MatButtonToggleModule, MatBadgeModule,
      MatBottomSheetModule,
      MatDialogModule,
      MatDividerModule,
      MatNativeDateModule,
      MatPaginatorModule,
      MatProgressBarModule,
      MatProgressSpinnerModule,
      MatRadioModule,
      MatSliderModule,
      MatSnackBarModule,
      MatSortModule,
      MatStepperModule,
      MatTableModule,
      MatTooltipModule,
      MatTreeModule],
})
export class MaterialModule {}
