import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';

import {UsersService} from '../../users.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.css']
})
export class AchievementsComponent implements OnInit {

  achieve;
  constructor(private usersService: UsersService,
    private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.usersService.achievements().subscribe(
      res => {
          // console.log(res['achieve']);
          // console.log('success');
          this.achieve = res['achieve'];

          // console.log(this.achieve[1].name);
          // console.log(this.achieve.length);
      },
      err => {
        // alert('called');
        console.log('error  ');
      }
    );

  }

}
