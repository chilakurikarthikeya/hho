import { Component, OnInit } from '@angular/core';

import {Router, RouterModule} from '@angular/router';
import {UsersService} from '../../users.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class  UserProfileComponent implements OnInit {

  userDetails;
  constructor(private router: Router, private usersService: UsersService) { }

  ngOnInit() {
    this.usersService.getUserProfile().subscribe(
        res => {
          this.userDetails = res['rep'];

          if (this.userDetails.otp == false) {
            // alert('in profile' + this.userDetails.otp);
            this.router.navigateByUrl('/otp_verify');
          } else {
              this.usersService.otp_verify = true;
          }

        },
        err => {

        }
    );
  }
  onLogout() {
    this.usersService.deleteToken();
    this.router.navigate(['/login']);

  }

}
