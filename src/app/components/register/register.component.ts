import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';

import {Router, RouterModule} from '@angular/router';
import {UsersService} from '../../users.service';

// spinner loading
import { NgxSpinnerService } from 'ngx-spinner'


export interface Branch {
  value: string;
  viewValue: string;
}
export interface Designation {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  showSucessMessage: boolean;
  serverErrorMessages: string;
    hide = true;

  HRForm;
  hrSubmitted = false;
  StuForm;
  StuSubmitted = false;
  OtherForm;
  OtherSubmitted = false;

     // Branch
     branchs: Branch[] = [
      {value: 'CSE', viewValue: 'CSE'},
      {value: 'ECE', viewValue: 'ECE'},
      {value: 'CIVIL', viewValue: 'CIVIL'},
      {value: 'MECH', viewValue: 'MECH'},
      {value: 'MME', viewValue: 'MME'},
      {value: 'CHEM', viewValue: 'CHEM'}
    ];
    // Branch
    desig: Designation[] = [
      {value: 'Core', viewValue: 'Core'},
      {value: 'Accountant', viewValue: 'Accountant'},
      {value: 'Executive', viewValue: 'Executive'},
      {value: 'Representative', viewValue: 'Representative'}
    ];
  // mail validation
  // email = new FormControl('', [Validators.required, Validators.email]);
  // getErrorMessage() {
  //   return this.email.hasError('required') ? 'You must enter a value' :
  //       this.email.hasError('email') ? 'Not a valid email' :
  //           '';
  // }



  constructor( private usersService: UsersService, private router: Router, private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService) {}

  ngOnInit() {
    if (this.usersService.isLoggedIn()) {
      this.router.navigateByUrl('/user_profile');
    }

     // stu
      this.StuForm = this.formBuilder.group({
        fullname: ['', Validators.required],
        id: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        ph_no: ['', [Validators.required, Validators.minLength(10)]],
        class: ['', [Validators.required]],
        branch: ['', Validators.required ],
        password: ['', Validators.required],
        c_password: ['', Validators.required]
    });
    // hr
    this.HRForm = this.formBuilder.group({
      fullname: ['', Validators.required],
      id: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      ph_no: ['', [Validators.required, Validators.minLength(10)]],
      class: ['', [Validators.required]],
      branch: ['', Validators.required ],
      designation: ['', Validators.required],
      date_of_joining: ['', Validators.required],
      moto_of_joining: ['', Validators.required],
      password: ['', Validators.required],
      c_password: ['', Validators.required]
  });

  //  Others
  this.OtherForm = this.formBuilder.group({
    fullname: ['', Validators.required],
    id: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    ph_no: ['', [Validators.required, Validators.minLength(10)]],
    password: ['', Validators.required],
    c_password: ['', Validators.required]
});
  }

  // get f() { return this.HRForm.controls; }

  onHrSubmit() {
    this.hrSubmitted = true;
    console.log(this.HRForm.value);

    if (this.HRForm.invalid) {
      alert('fill all details');
      return;
    } else {
      this.spinner.show();

        this.usersService.registerHR(this.HRForm.value).subscribe(
          res => {
            alert('Registred successfully');
            this.router.navigateByUrl('/login');
          },
          err => {
            this.spinner.hide();
            if (err.status === 422) {
              alert('Email/id already exists');
            } else {
                alert('reg Failed');
            }
          }
        );

    }
  }

  // Stu aubmit
  onStuSubmit() {
    this.StuSubmitted = true;
    console.log(this.HRForm.value);

    if (this.StuForm.invalid) {
      alert('fill all details');
      return;
    } else {

        this.usersService.registerStu(this.StuForm.value).subscribe(
          res => {
            alert('Registred successfully');
            this.router.navigateByUrl('/login');
          },
          err => {
            if (err.status === 422) {
              alert('Email/id already exists');
            } else {
                alert('reg Failed');
            }
          }
        );

    }
  }

  // Otehr submit
  onOtherSubmit() {
    this.OtherSubmitted = true;
    console.log(this.HRForm.value);

    if (this.OtherForm.invalid) {
      alert('fill all details');
      return;
    } else {

        this.usersService.registerOther(this.OtherForm.value).subscribe(
          res => {
            alert('Registred successfully');
            this.router.navigateByUrl('/login');
          },
          err => {
            if (err.status === 422) {
              alert('Email/id already exists');
            } else {
                alert('reg Failed');
            }
          }
        );

    }
  }


  // onSubmit(form: NgForm) {

  //   this.usersService.postUser(form.value).subscribe(
  //     res => {
  //       this.showSucessMessage = true;
  //       setTimeout(() => this.showSucessMessage = false, 4000);
  //       this.resetForm(form);
  //       alert('SUccess fully registred');
  //       this.router.navigateByUrl('/login');
  //       console.log(form);
  //     },

  //     err => {
  //       if (err.status === 422) {
  //         this.serverErrorMessages = err.error.join('<br/>');
  //         // this.resetForm(form);
  //         console.log(form.value);
  //         console.log('422');
  //       } else {
  //         this.serverErrorMessages = 'Something Went wrong';
  //         // this.resetForm(form);
  //         console.log(form.value);
  //       }
  //     }
  //   );

  // }
  // resetForm(form: NgForm) {
  //   this.usersService.selectedHR = {
  //     fullname: '',
  //   email: '',
  //   id: '',
  //   ph_no: '',
  //   class: '',
  //   branch: '',
  //   moto_of_joining: '',
  //   date_of_joining: '',
  //   designatio: '',
  //   password: '',
  //   c_password: ''

  //   };
  //   form.resetForm();
  //   this.serverErrorMessages = '';
  // }

  // myControl = new FormControl();
  // options: string[] = ['PUC-1', 'PUC-2',' CSE','ECE','CIVIl','MECH','MME','CHEM'];
  // animalControl = new FormControl('', [Validators.required]);
  // selectFormControl = new FormControl('', Validators.required);



}
