import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../users.service';


@Component({
  selector: 'app-problems',
  templateUrl: './problems.component.html',
  styleUrls: ['./problems.component.css']
})
export class ProblemsComponent implements OnInit {
  problems;
  status_update_option: String;
  status;


  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.usersService.problems_list().subscribe(
      res =>{
          this.problems = res['problems'];
      },
      err =>{

      }
    );
  }
  onclick(id){
    if(this.status_update_option === undefined ){
      alert('invalid option selected');
      return;

    } else {
      // alert(this.status);
        this.status = {
            status : this.status_update_option
        }

      this.usersService.problem_status_update(id, this.status).subscribe(
        res => {
            alert('successfully updated');
        },
        err => {
            alert(err);
        }
      );
    }

  }

}
