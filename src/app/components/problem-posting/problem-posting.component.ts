  import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder} from '@angular/forms';

import {Router, RouterModule} from '@angular/router';
import {UsersService} from '../../users.service';

import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';
import { take, finalize } from 'rxjs/operators';

export interface Branch {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-problem-posting',
  templateUrl: './problem-posting.component.html',
  styleUrls: ['./problem-posting.component.css']
})
export class ProblemPostingComponent implements OnInit {


  // Branch
  branchs: Branch[] = [
    {value: 'CSE', viewValue: 'CSE'},
    {value: 'ECE', viewValue: 'ECE'},
    {value: 'CIVIL', viewValue: 'CIVIL'},
    {value: 'MECH', viewValue: 'MECH'},
    {value: 'MME', viewValue: 'MME'},
    {value: 'CHEM', viewValue: 'CHEM'}
  ];

  
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;

  filename;
  doc_url;
  firebase_url;

  problemForm: FormGroup;
  submitted = false;
  image = ' ';


  constructor(private router: Router, private usersService: UsersService,
     private formBuilder: FormBuilder, private afStorage: AngularFireStorage) { }



     // handling upload

     upload(event) {
       this.filename = event.target.files[0].name;
      const id = 'bk' + this.filename;

      this.doc_url = id;

      this.ref = this.afStorage.ref(id);
      this.task = this.ref.put(event.target.files[0]);
      this.uploadProgress = this.task.percentageChanges();
      // this.uploadState = this.task.snapshotChanges().pipe(Map(s => s.state));
      this.uploadProgress = this.task.percentageChanges();
      // this.downloadURL = this.task.getdownloadURL();
      this.task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = this.ref.getDownloadURL();
          this.downloadURL.subscribe(url => (this.image = url));
        })
      )
      .subscribe();
      console.log(this.image);

    //   this.task = this.ref.put(event.target.files[0]);

    //   const downloadURL = this.ref.getDownloadURL().subscribe(url => {
    //     const Url = url; // for ts
    //     this.firebase_url = url; // with this you can use it in the html
    //     console.log(Url);
    // });

    //   // console.log('download load:' + this.downloadURL);
    //   this.problemForm.value.doc_path = this.firebase_url;

    //   console.log(this.firebase_url);
    }


  ngOnInit() {

    this.problemForm = this.formBuilder.group({
      Victim_Name: ['', [Validators.required]],
      id_no: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      ph_no: ['', [Validators.required]],
      branch: ['', [Validators.required]],
      class: ['', [Validators.required]],
      problem: ['', [Validators.required]],
      problem_description: ['', [Validators.required]],
      required_amount: ['', [Validators.required]],
      doc_path: [this.image, [Validators.required]]
    });
  }
// convenience getter for easy access to form fields
get f() { return this.problemForm.controls; }



    onpost() {

console.log(this.problemForm.value.doc_path);
    

      console.log('called onpost');
      this.problemForm.value.doc_path = this.image;
      this.submitted = true;

        if (this.problemForm.invalid) {
          
          console.log( this.problemForm.value);
          console.log('error');
          return;
        }

        console.log( 'with out false' + this.problemForm.value);

        this.usersService.problempost(this.problemForm.value).subscribe(
          res => {
              alert('successfully posted');
          },
          err => {
             alert('error while posting problem');
          }
        );
    }
}
