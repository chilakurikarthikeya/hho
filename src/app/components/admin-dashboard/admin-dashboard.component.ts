import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  

  constructor( private userservice: UsersService, private router: Router ) { }

  ngOnInit() {

    this.userservice.admin_verify().subscribe(
      res => {
        alert('You are  authorised to login');

      },
      err => {
        alert('You are not authorised to login');
        this.router.navigateByUrl('/home');
      }
    );
  }

}
