import { Component, OnInit } from '@angular/core';

import { ViewChild } from '@angular/core';
import {UsersService} from '../../users.service';

// for table
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}
/** Constants used to fill up our data base. */
// const COLORS: string[] = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
//   'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
// const NAMES: string[] = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
//   'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
//   'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];

const COLORS: string[] = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
  'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
const NAMES: string[] = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
  'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
  'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];


@Component({
  selector: 'app-accountinfo',
  templateUrl: './accountinfo.component.html',
  styleUrls: ['./accountinfo.component.css']
})
export class AccountinfoComponent implements OnInit {

    accinfo;
    acc_balance;

  displayedColumns: string[] = ['SNo', 'Date', 'Batch', 'Branch', 'Class', 'purpose', 'Collected_Amount'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor( private usersService: UsersService) {
    // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // console.log(users);
  


    // // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
  }




  ngOnInit() {

      this.usersService.accinfo().subscribe(
        res => {
          // alert('success');
            this.accinfo = res['accinfo'];
            const users = this.accinfo;

      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(users);
            // console.log(this.accinfo);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

        },
        err => {
          // console.log(err);
          alert('error');
        }
      );

      //get acc balance 

      this.usersService.getacc_balance().subscribe(
        res => {
            this.acc_balance = res['accbalance'];
            this.usersService.acc_balance = this.acc_balance;
            console.log(this.acc_balance);
        },
        err => {
          this.acc_balance = 'error while loading balabce';
          this.usersService.acc_balance = this.acc_balance;

        }
      );



  }
 
  

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}


function createNewUser(id: number): UserData {
  const name =
      NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

      // console.log( NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      // NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.');

  return {
    id: id.toString(),
    name: name,
    progress: Math.round(Math.random() * 100).toString(),
    color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
  };
}


