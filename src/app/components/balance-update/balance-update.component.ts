import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';
import { Router } from '@angular/router';

import {UsersService} from '../../users.service';


@Component({
  selector: 'app-balance-update',
  templateUrl: './balance-update.component.html',
  styleUrls: ['./balance-update.component.css']
})
export class BalanceUpdateComponent implements OnInit {
  acc_balance;
  moneyForm;
  submitted = false;

  constructor(private usersService: UsersService,
    private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    // this.acc_balance = this.userservice.acc_balance;
    // console.log(this.acc_balance);
    this.moneyForm = this.formBuilder.group({
        money: ['', Validators.required]
    });
  }
  get f(){return this.moneyForm.controls; }

  onpost(){

    this.submitted = true;
    if(this.moneyForm.invalid){
      return ;
    }
    this.usersService.updateacc_balance(this.moneyForm.value).subscribe(
      res => {
          alert('successfully updated');
      },
      err =>{
        alert('erroe while updating');
      }

    );
  }

}
