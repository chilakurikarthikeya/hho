import { Component, OnInit } from '@angular/core';

import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';

import {Router, RouterModule} from '@angular/router';
import {UsersService} from '../../users.service';



@Component({
  selector: 'app-accountinfo-post',
  templateUrl: './accountinfo-post.component.html',
  styleUrls: ['./accountinfo-post.component.css']
})
export class AccountinfoPostComponent implements OnInit {

  accinfoForm: FormGroup;
  submitted = false;


  constructor(private usersService: UsersService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.accinfoForm = this.formBuilder.group({
        date: ['', Validators.required],
        batch: ['', Validators.required],
        branch: ['', Validators.required],
        class: ['', Validators.required],
        purpose: ['', Validators.required],
        collected_amount: ['', Validators.required]
    });
  }
  get f() { return this.accinfoForm.controls; }



  accinfo_post() {


    if (this.accinfoForm.invalid ) {
      // console.log(this.accinfoForm.value);
       return;
    }

    console.log(this.accinfoForm.value);
    this.usersService.accinfo_post(this.accinfoForm.value).subscribe(
      res => {
            alert('success');
            // this.accinfoForm.reset();
      },
      err => {
          alert('failure');
      }
    );

  }
}
