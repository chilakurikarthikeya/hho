import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountinfoPostComponent } from './accountinfo-post.component';

describe('AccountinfoPostComponent', () => {
  let component: AccountinfoPostComponent;
  let fixture: ComponentFixture<AccountinfoPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountinfoPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountinfoPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
