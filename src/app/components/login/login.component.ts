import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';
      
import { ReactiveFormsModule } from '@angular/forms';
//spinner loading
import { NgxSpinnerService } from 'ngx-spinner';


import {Router, RouterModule} from '@angular/router';
import {UsersService} from '../../users.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
    submitted = false;
    hide = true;
    disabled = false;

    loading = false;


  // getErrorMessage() {
  //   return this.email.hasError('required') ? 'You must enter a value' :
  //       this.email.hasError('email') ? 'Not a valid email' :
  //           '';
  // }
  // getErrorMessage1() {
  //   return this.password.hasError('required') ? 'You must enter a value' :
  //       // this.email.hasError('email') ? 'Not a valid email' :
  //           '';
  // }

  constructor(private router: Router, private usersService: UsersService, private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService) { }
  




  ngOnInit() {


  
    if ( this.usersService.isLoggedIn()) {

      this.router.navigateByUrl('/user_profile');
    }
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
  
  

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }



  onlogin() {
    this.submitted = true;
    this.loading = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      this.loading = false;
        return;
    }

    this.spinner.show();
    this.usersService.login(this.loginForm.value).subscribe(
          res => {
            this.usersService.setToken(res['token']);
            this.router.navigateByUrl('/user_profile');
            // console.log(form.value);
          },
          err => {


              // this.serverErrorMessages = err.console.error.message;
              alert('error while loging in');
              this.spinner.hide();
              // console.log(form.value);
          }
        );

    
}
  //   this.SigninForm = this.formBuilder.group({
  //       email: ['', Validators.required, Validators.email],
  //       password: ['', Validators.required]
  //   });
  // }
  // get f() { return this.SigninForm.controls; }



  // onlogin() {
  //   // alert(this.SigninForm.value);
  //   this.SigninSubmitted = true;
  //   // if (this.SigninForm.invalid) {
  //   //   alert('fill all details');
  //   //   return;
  //   // }
  //   //  else
  //     {
  //   this.usersService.login(this.SigninForm.value).subscribe(
  //     res => {
  //       this.usersService.setToken(res['token']);
  //       this.router.navigateByUrl('/user_profile');
  //       // console.log(form.value);
  //     },
  //     err => {
  //         this.serverErrorMessages = err.console.error.message;
  //         alert('error while loging in');
  //         // console.log(form.value);

  //     }
  //   );
  //   }
  // }

  }

