import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';

import {UsersService} from '../../users.service';

// firebase
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';

import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { take, finalize } from 'rxjs/operators';

// import { url } from 'inspector';
// import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-achievement-post',
  templateUrl: './achievement-post.component.html',
  styleUrls: ['./achievement-post.component.css']
})
export class AchievementPostComponent implements OnInit {

  // title = 'app';
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  // for upload proggress
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
 
  url1 = '';
  image=' ' ;
// form start
achieveForm: FormGroup;
submitted = false;


  constructor(private afStorage: AngularFireStorage, private usersService: UsersService,
     private router: Router, private formBuilder: FormBuilder) { }

  upload(event) {
    this.url1 = event.target.files[0].name;
    const id = this.url1;

    this.ref = this.afStorage.ref(id);
      this.task = this.ref.put(event.target.files[0]);
      this.uploadProgress = this.task.percentageChanges();
      // this.uploadState = this.task.snapshotChanges().pipe(Map(s => s.state));
      this.uploadProgress = this.task.percentageChanges();
      // this.downloadURL = this.task.getdownloadURL();
      this.task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = this.ref.getDownloadURL();
          this.downloadURL.subscribe(url => (this.image = url));
        })
      )
      .subscribe();
      console.log('fire base url::' + this.image);

    // for image preview
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url1 = event.target.result;
      };
    }
  }

  ngOnInit() {
    this.achieveForm = this.formBuilder.group({ 
      name: ['', Validators.required],
      problem: ['', Validators.required],
      amount_donated: ['', Validators.required],
      date_of_donating: ['', Validators.required],
      imageUrl: [this.image, Validators.required],
    });
  }

  get f() { return this.achieveForm.controls; }

  onpost() {

    console.log('in on post fire base url::' + this.image);
    this.achieveForm.value.imageUrl = this.image;
    // this.imageurl = this.image;
    this.submitted = true;

    if (this.achieveForm.invalid) {
        console.log(this.achieveForm.value);
      // alert('gusss...');
      return;
    } 

    this.usersService.achievepost(this.achieveForm.value).subscribe(
      res => {
          alert('successfully posted');
      },
      err => {
        alert('error while posting');
      }
    );
  }

}
