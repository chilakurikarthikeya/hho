import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementPostComponent } from './achievement-post.component';

describe('AchievementPostComponent', () => {
  let component: AchievementPostComponent;
  let fixture: ComponentFixture<AchievementPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchievementPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
