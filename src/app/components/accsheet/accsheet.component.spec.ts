import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccsheetComponent } from './accsheet.component';

describe('AccsheetComponent', () => {
  let component: AccsheetComponent;
  let fixture: ComponentFixture<AccsheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccsheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
