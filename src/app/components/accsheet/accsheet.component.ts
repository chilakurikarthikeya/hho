import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../users.service';
import {Router} from '@angular/router';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';




@Component({
  selector: 'app-accsheet',
  templateUrl: './accsheet.component.html',
  styleUrls: ['./accsheet.component.css']
})
export class AccsheetComponent implements OnInit {
  accsheet;
  accsheetupdate_Form;
  submitted = false;

  constructor(private userservice: UsersService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.userservice.accsheet_get().subscribe(
      res => {
          this.accsheet = res['accsheet'];
      },
      err => {
          this.accsheet = err;
      }
    );

    // sheet update form
      this.accsheetupdate_Form = this.formBuilder.group({
        purpose: ['', Validators.required],
        amount: [, Validators.required]

      });
  }
  get f() { return this.accsheetupdate_Form.controls; }

  onpost(){

      this.submitted = true;
      // this.accsheetupdate_Form.amount.value = parseInt(this.accsheetupdate_Form.value);
      if (this.accsheetupdate_Form.invalid){
        return;
      }
      this.userservice.accsheet_post(this.accsheetupdate_Form.value).subscribe(
        res => {
            alert('successfully updated');
        },
        err =>{
          alert('error while updating');
        }
      )
  }


}
