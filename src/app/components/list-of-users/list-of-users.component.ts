import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../users.service';


@Component({
  selector: 'app-list-of-users',
  templateUrl: './list-of-users.component.html',
  styleUrls: ['./list-of-users.component.css']
})
export class ListOfUsersComponent implements OnInit {

  hrs;
  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.usersService.hrs_list().subscribe(
      res => {
          // console.log(res['achieve']);
          // console.log('success');
          this.hrs = res['rep'];

          // console.log(this.achieve[1].name);
          console.log(this.hrs);
      },
      err => {
        console.log('error  ');
      }
    );
  }

  onclick(id){
    this.usersService.approve_hr(id).subscribe(
      res => {
        alert('approved successfully');
      },
      err => {
          alert('error while approving');
      }
    )
  }

}
