import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';
import { Router } from '@angular/router';

import {UsersService} from '../../users.service';

@Component({
  selector: 'app-todayspecial-post',
  templateUrl: './todayspecial-post.component.html',
  styleUrls: ['./todayspecial-post.component.css']
})
export class TodayspecialPostComponent implements OnInit {
  specialform;
  submitted = false;

  constructor(  private usersService: UsersService,
    private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.specialform = this.formBuilder.group({
        name: ['', Validators.required],
        date: ['', Validators.required],
        description: ['', Validators.required]
    });
  }
  get f() { return this.specialform.controls; }


  onpost() {

    this.submitted = true;

    if (this.specialform.invalid) {

      return;
    }
    console.log(this.specialform.valus)
;
    this.usersService.todayspecial_post(this.specialform.value).subscribe(
      res => {
          alert('successfully posted');
      },
      err => {
        alert('erroe while posting');
      }
    );
  }


}
