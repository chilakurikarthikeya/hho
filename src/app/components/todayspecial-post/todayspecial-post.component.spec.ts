import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodayspecialPostComponent } from './todayspecial-post.component';

describe('TodayspecialPostComponent', () => {
  let component: TodayspecialPostComponent;
  let fixture: ComponentFixture<TodayspecialPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodayspecialPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodayspecialPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
