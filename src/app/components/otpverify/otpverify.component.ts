import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';

import {Router, RouterModule} from '@angular/router';
import {UsersService} from '../../users.service';
import { send } from 'q';
// import {UserProfileComponent} from '../user-profile/user-profile.component';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import {MatSnackBar} from '@angular/material'; // snack bar


@Component({
  selector: 'app-otpverify',
  templateUrl: './otpverify.component.html',
  styleUrls: ['./otpverify.component.css']
})
export class OtpverifyComponent implements OnInit {

  hide = false;
  otp;
  otp_send = false;
  send1;
  load = false; // for loading

  otpForm: FormGroup;
  submitted = false;

  constructor( private usersService: UsersService, private router: Router, private formBuilder: FormBuilder,
    private snackBar: MatSnackBar ) { }


      // for snack bar

      openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
          duration: 9000,
        });
      }
      // private UserProfile: UserProfileComponent
  ngOnInit() {

    // console.log( Math.floor(Math.random() * 9999) + 1000 );
    if (this.usersService.otp_verify == true) {
          this.router.navigateByUrl('/user_profile');
    }

    this.otpForm = this.formBuilder.group({
        entered_otp: ['', [Validators.required]]
    });
  }

// convenience getter for easy access to form fields
get f() { return this.otpForm.controls; }


  // sending mail for otp
  send_mail() {

    this.load = true; // toactivate loaading

    this.otp = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
    this.send1 = {
      otp1: 12
  };
    this.send1.otp1 = this.otp;
    console.log(this.otp);
    console.log('called');
    this.usersService.send_mail(this.send1).subscribe(
      res => {
          this.otp_send = true;
          // alert('otp sent to your registed mail');
          this.hide = true;
          this.load = false;
          this.openSnackBar('otp  successfully sent to your registred mail', 'ok');
      },
      err => {

            // alert('OTP sending  Failed');
            this.load = false;
            this.openSnackBar('error while sending otp', 'ok');
      }
    );
  }

  // set opt to true in user db

  set_otp() {
    console.log('set otp called');
    this.submitted = true;
    if (this.otpForm.invalid) {

        return;
    }
    if (this.otp_send === false || this.otpForm.value.entered_otp != this.send1.otp1) {
          alert('invalid Otp');
          return;
    }
    // alert(this.otpForm.value.entered_otp);
    this.usersService.otp().subscribe(
      res => {
        alert('OTP Success fully verifyed');
          this.router.navigateByUrl('/home');
      },
      err => {
          alert('while verifying OTP');
      }
    );

  }



}
